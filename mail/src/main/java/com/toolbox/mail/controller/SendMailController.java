package com.toolbox.mail.controller;

import com.toolbox.mail.service.SendEmailHandler;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description: TODO
 * @Author qiang_xue
 * @Date 2022/7/27 16:26
 * @Version 1.0
 */
@RestController
@AllArgsConstructor
@RequestMapping("/mail")
public class SendMailController {

    private final SendEmailHandler createMailMessage;

    @GetMapping("/send")
    public String send(){

        return "";
    }
}
