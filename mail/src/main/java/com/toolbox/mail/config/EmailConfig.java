package com.toolbox.mail.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

/**
 * @Description: TODO
 * @Author qiang_xue
 * @Date 2022/7/28 16:35
 * @Version 1.0
 */
@Data
@Component
@Configuration
public class EmailConfig {

    @Value("${mail.fromMail.username}")
    private String  userName;
    @Value("${mail.fromMail.password}")
    private String  password;
    @Value("${mail.fromMail.host}")
    private String  host;
    @Value("${mail.fromMail.fromAddr}")
    private String  fromAddr;

    @Bean
    public JavaMailSenderImpl javaMailSender() {

        JavaMailSenderImpl senderImpl = new JavaMailSenderImpl();
        // 设定mail server
        senderImpl.setHost(host);
        return senderImpl;
    }
}
