package com.toolbox.mail.vo;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @Description: TODO
 * @Author qiang_xue
 * @Date 2022/7/28 14:37
 * @Version 1.0
 */
@Data
@Builder
public class EmailData {
    // 邮件发送列表
    private List<String> toList;
    // 邮件抄送列表
    private List<String> ccList;
    // 邮件主题
    private String subject;
    // 邮件内容
    private String Text;
    //附件名称
    private String attachmentName;
    //附件路径
    private String attachmentPath;
}
