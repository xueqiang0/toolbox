package com.toolbox.mail.service.impl;

import com.toolbox.mail.config.EmailConfig;
import com.toolbox.mail.service.SendEmailService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.List;

/**
 * @Description: TODO
 * @Author qiang_xue
 * @Date 2022/8/3 14:06
 * @Version 1.0
 */
@Service
@RequiredArgsConstructor
public class SendEmailServiceImpl implements SendEmailService {

    protected Logger logger = LoggerFactory.getLogger(SendEmailServiceImpl.class);
    private final JavaMailSender mailSender;
    private final JavaMailSender javaMailSender;
    private final EmailConfig emailConfig;

    @Override
    public void sendSimpleMail(List<String> toList, List<String> ccList, String subject, String content) {

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(emailConfig.getFromAddr());
        message.setTo(toList.toArray(new String[] {}));
        message.setCc(ccList.toArray(new String[] {}));
        message.setSubject(subject);
        message.setText(content);
        try {
            mailSender.send(message);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }

    @Override
    public void sendAttchMail(List<String> toList, List<String> ccList, String subject, String context) {
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            //下面的这个方法很重要啊，如果需要加附件传递，记得开启multipart为true
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom(emailConfig.getFromAddr());
            helper.setTo(toList.toArray(new String[] {}));
            helper.setCc(ccList.toArray(new String[] {}));
            helper.setSubject(subject);
            helper.setText(context);

            //添加附件
            String filePath = "E:\\helloworld.txt";
            FileSystemResource file = new FileSystemResource(new File(filePath));
            String fileName = filePath.substring(filePath.lastIndexOf(File.separator));
            helper.addAttachment(fileName, file);
            javaMailSender.send(mimeMessage);
            logger.info("Successfully!");
        } catch (Exception e) {
            logger.info("Filed!"+ e);
        }
    }
}
