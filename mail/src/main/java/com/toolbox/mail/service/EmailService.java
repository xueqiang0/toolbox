package com.toolbox.mail.service;

import javax.mail.MessagingException;

/**
 * @Description: TODO
 * @Author qiang_xue
 * @Date 2022/8/1 16:07
 * @Version 1.0
 */
public interface EmailService {
    /**
     * 邮件前置处理
     */
    void payDataEmail() throws MessagingException;
}
