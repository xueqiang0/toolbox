package com.toolbox.mail.service.impl;

import com.toolbox.mail.config.EmailConfig;
import com.toolbox.mail.service.SendEmailHandler;
import com.toolbox.mail.vo.EmailData;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Properties;
import org.springframework.mail.javamail.MimeMessageHelper;

@Service
@RequiredArgsConstructor
public class SendEmailHandlerImpl implements SendEmailHandler {

    private final JavaMailSenderImpl javaMailSenderImpl;
    private final EmailConfig emailConfig;

    public void sendEmail(EmailData emailData) throws MessagingException {

        MimeMessage mailMessage = javaMailSenderImpl.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(mailMessage, true, "utf-8");

        boolean rtn = true;

        //添加附件
        FileSystemResource file = new FileSystemResource(new File(emailData.getAttachmentPath()));
        messageHelper.addAttachment(emailData.getAttachmentName(), file);
        // 设置收件人，寄件人 用数组发送多个邮件
        if (emailData.getToList() != null) {
            if (emailData.getToList().size() != 0) {
                String[] array = emailData.getToList().toArray(new String[] {});
                messageHelper.setTo(array);
                rtn = false;
            }
        }

        if (emailData.getCcList() != null) {
            if (emailData.getCcList().size() != 0) {
                String[] array = emailData.getCcList().toArray(new String[] {});
                messageHelper.setCc(array);
                rtn = false;
            }
        }

        if (rtn) {
            return;
        }

        messageHelper.setFrom(emailConfig.getFromAddr());
        if (emailData.getSubject() == null) {
            //messageHelper.setSubject(emailConfig.getServerType());
        }

        if (emailData.getText() == null) {
            messageHelper.setText("", true);
        } else {
            messageHelper.setText(emailData.getText(), true);
        }
        javaMailSenderImpl.setUsername(emailConfig.getUserName()); // 根据自己的情况,设置username
        javaMailSenderImpl.setPassword(emailConfig.getPassword()); // 根据自己的情况, 设置password

        Properties prop = new Properties();
        prop.put("mail.smtp.auth", "true"); // 将这个参数设为true，让服务器进行认证,认证用户名和密码是否正确
        prop.put("mail.smtp.timeout", "25000");
        javaMailSenderImpl.setJavaMailProperties(prop);

        // 发送邮件
        javaMailSenderImpl.send(mailMessage);

    }
}