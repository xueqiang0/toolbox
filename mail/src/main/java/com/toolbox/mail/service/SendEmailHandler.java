package com.toolbox.mail.service;

import com.toolbox.mail.vo.EmailData;

import javax.mail.MessagingException;

public interface SendEmailHandler {
    /**
     * 发送邮件
     */
    void sendEmail(EmailData emailData) throws MessagingException;
}
