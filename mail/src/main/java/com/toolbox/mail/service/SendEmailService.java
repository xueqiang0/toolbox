package com.toolbox.mail.service;

import java.util.List;

/**
 * @Description: TODO
 * @Author qiang_xue
 * @Date 2022/8/3 14:06
 * @Version 1.0
 */
public interface SendEmailService {
    void sendSimpleMail(List<String> toList, List<String> ccList, String subject, String content);
    void sendAttchMail(List<String> toList, List<String> ccList, String subject, String context);

}
