package com.toolbox.mail.service.impl;

import com.toolbox.mail.service.EmailService;
import com.toolbox.mail.service.SendEmailHandler;
import com.toolbox.mail.vo.EmailData;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Description: TODO
 * @Author qiang_xue
 * @Date 2022/8/1 16:13
 * @Version 1.0
 */
@Service
@AllArgsConstructor
public class EmailServiceImpl implements EmailService {

    private final static String subject = "付费数据";
    private final static String attachmentName = "file:///c:/";
    private final static String attachmentPath = "付费数据.xlsx";

    private final SendEmailHandler sendEmailHandler;

    @Override
    public void payDataEmail() throws MessagingException {

        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        List<String> toList = new ArrayList<>();
        List<String> ccList = new ArrayList<>();
        EmailData emailData = EmailData.builder()
                .toList(toList)
                .ccList(ccList)
                .subject(format.format(date)+subject)
                .Text("")
                .attachmentName(format.format(date)+attachmentName)
                .attachmentPath(attachmentPath)
                .build();

        sendEmailHandler.sendEmail(emailData);
    }
}
